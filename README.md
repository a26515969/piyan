# label
PPE detection using yolo model

![流程圖](Pic_data/PM2.png)

## Priority
1.加強多人場景識別


## Progress

Web_link will be added to each slice after all video be uploaded.

Use &#10004; {Checker} to show the status of each video, and who did the job. 

last modified:Finn 2024/7/29.

Latest modification: Finn 2024/10/11.


<table border="1">
   <tr>
      <th align="center" valign="center"><b>class</b></th>
      <th align="center" valign="center"><b>last instance</b></th>
      <th align="center" valign="center"><b>latest instance</b></th>
      <th align="center" valign="center"><b>note</b></th>
   </tr>
   <tr>
      <td align="center" valign="center">all</td>
      <td align="center" valign="center">57759</td> <!-- 填写实际张数 -->
      <td align="center" valign="center">57870</td>
      <td align="center" valign="center"></td>
   </tr>
   <tr>
      <td align="center" valign="center">helmet</td>
      <td align="center" valign="center">11216</td> <!-- 填写实际张数 -->
      <td align="center" valign="center">11644</td>
      <td align="center" valign="center"></td>
   </tr>
   <tr>
      <td align="center" valign="center">no-helmet</td>
      <td align="center" valign="center">7838</td> <!-- 填写实际张数 -->
      <td align="center" valign="center">7842</td>
      <td align="center" valign="center"></td>
   </tr>
   <tr>
      <td align="center" valign="center">vest</td>
      <td align="center" valign="center">8428</td> <!-- 填写实际张数 -->
      <td align="center" valign="center">8664</td>
      <td align="center" valign="center"></td>
   </tr>
   <tr>
      <td align="center" valign="center">no-vest</td>
      <td align="center" valign="center">9745</td> <!-- 填写实际张数 -->
      <td align="center" valign="center">9753</td>
      <td align="center" valign="center"></td>
   </tr>
   <tr>
      <td align="center" valign="center">person</td>
      <td align="center" valign="center">20532</td> <!-- 填写实际张数 -->
      <td align="center" valign="center">20851</td>
      <td align="center" valign="center"></td>
   </tr>
</table>

# pt_file version

 yolov5m

![流程圖](Pic_data/yolov5m.png)

 yolov5n

![流程圖](Pic_data/yolov5n.png)



