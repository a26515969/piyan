
![流程圖](pic_data.png)

<table border="1">
   <tr>
      <th width="120" align="center" valign="center"><b>            file name</b></th><!-- 空4個tab -->
      <th align="center" valign="center"><b>張數(已使用)</b></th>
      <th width="30" align="center" valign="center"><b>1st Check</b></th>
      <th width="30" align="center" valign="center"><b>2nd Check</b></th>
      <th align="center" valign="center"><b>                    note</b></th><!-- 空4個tab -->
   </tr>
   <!-- 以下是数据行，根据需要复制和填充 -->
   <tr>
      <td align="center" valign="center">白光區A字梯作業</td>
      <td align="center" valign="center">407(407)</td> <!-- 填写实际张数 -->
      <td align="center" valign="center">&#10004; Finn</td><!-- 填写实际张数 -->
      <td align="center" valign="center"> </td>
      <td align="center" valign="center">all 407 a_frame pic to training data</td> <!-- 填写备注信息 -->
   </tr>
    <tr>
      <td align="center" valign="center">誤判_AI-PPE_2023_04_07_11上午_19_22</td>
      <td align="center" valign="center">1552(1141)</td> <!-- 填写实际张数 -->
      <td align="center" valign="center">&#10004; Finn</td>
      <td align="center" valign="center"> </td>
      <td align="center" valign="center">frame_0000-frame_1141 to training data<br>
                                         frame_1142 missing</td> <!-- 填写备注信息 -->
   </tr>
   <tr>
      <td align="center" valign="center">construction safety.v2-release.yolov5pytorch(patch1)</td>
      <td align="center" valign="center">997(997)</td> <!-- 填写实际张数 -->
      <td align="center" valign="center">&#10004; Finn</td>
      <td align="center" valign="center"> </td>
      <td align="center" valign="center">all 997 pic to training data</td> <!-- 填写备注信息 -->
   </tr>
   <tr>
      <td align="center" valign="center">Personal Protection Detetction.v3i.yolov5pytorch(patch2)</td>
      <td align="center" valign="center">1300(1300)</td> <!-- 填写实际张数 -->
      <td align="center" valign="center">&#10004; Finn</td>
      <td align="center" valign="center"> </td>
      <td align="center" valign="center">all 1300 pic to training data</td> <!-- 填写备注信息 -->
   </tr>
   <tr>
      <td align="center" valign="center">Data20210916-slice001(patch3)</td>
      <td align="center" valign="center">420(420)</td> <!-- 填写实际张数 -->
      <td align="center" valign="center">&#10004; Finn</td>
      <td align="center" valign="center"> </td>
      <td align="center" valign="center">all 420 pic to training data</td> <!-- 填写备注信息 -->
   </tr>
   <tr>
      <td align="center" valign="center">resize640_allclasses_noaugs.yolov5pytorch(patch4)</td>
      <td align="center" valign="center">495(0)</td> <!-- 填写实际张数 -->
      <td align="center" valign="center"></td>
      <td align="center" valign="center"></td>
      <td align="center" valign="center"></td> <!-- 填写备注信息 -->
   </tr>
   <tr>
      <td align="center" valign="center">Person.v6i.yolov5pytorch(patch5)</td>
      <td align="center" valign="center">819(819)</td> <!-- 填写实际张数 -->
      <td align="center" valign="center">&#10004; Finn</td>
      <td align="center" valign="center"></td>
      <td align="center" valign="center">all 819 pic to training data</td> <!-- 填写备注信息 -->
   </tr>
   <tr>
      <td align="center" valign="center">office</td>
      <td align="center" valign="center">31(31)</td> <!-- 填写实际张数 -->
      <td align="center" valign="center">&#10004; Finn</td>
      <td align="center" valign="center"></td>
      <td align="center" valign="center">all 31 office_frame pic to training data</td> <!-- 填写备注信息 -->
   </tr>
   <tr>
      <td align="center" valign="center">中華工地(多人)</td>
      <td align="center" valign="center">3220(489)</td> <!-- 填写实际张数 -->
      <td align="center" valign="center"></td>
      <td align="center" valign="center"></td>
      <td align="center" valign="center">many_frame_0000-many_frame_2400，many_frame_2715-many_frame_5205 to training data<br>
                                         many_frame_2415-many_frame_2700 missing</td> <!-- 填写备注信息 -->
   </tr>
   <tr>
      <td align="center" valign="center">中華工地(戶外)</td>
      <td align="center" valign="center">297(297)</td> <!-- 填写实际张数 -->
      <td align="center" valign="center">&#10004; Finn</td>
      <td align="center" valign="center"></td>
      <td align="center" valign="center">all 297 outdoor_frame pic to training data<br><!-- 填写备注信息 -->
                                         
   </tr>
   <tr>
      <td align="center" valign="center">中華工地(戶外2)</td>
      <td align="center" valign="center">599(360)</td> <!-- 填写实际张数 -->
      <td align="center" valign="center"></td>
      <td align="center" valign="center"></td>
      <td align="center" valign="center">outdoor2_frame_0015-outdoor_frame_5400 to training data</td> <!-- 填写备注信息 -->
   </tr>
   <tr>
      <td align="center" valign="center">北部工地測試</td>
      <td align="center" valign="center">751(361)</td> <!-- 填写实际张数 -->
      <td align="center" valign="center"></td>
      <td align="center" valign="center"></td>
      <td align="center" valign="center">馥鴻 0000-1800 to validation data<br>
                                         馥鴻 2100-2685 to validation data<br>
                                         馥鴻 3030-3900 to validation data<br>
                                         馥鴻 4380-4800 to validation data<br>
                                         馥鴻 5535-6600 to validation data<br>
                                         馥鴻 7005-7290 to validation data<br>
                                         馥鴻 8400-8680 to validation data</td> <!-- 填写备注信息 -->
   </tr>
   <tr>
      <td align="center" valign="center">得正-基隆-001_2023_11_26_12下午_30_30</td>
      <td align="center" valign="center"></td> <!-- 填写实际张数 -->
      <td align="center" valign="center"></td>
      <td align="center" valign="center"></td>
      <td align="center" valign="center">for test </td> <!-- 填写备注信息 -->
   </tr>
   <tr>
      <td align="center" valign="center">得正-基隆-007_2023_11_26_12下午_30_30</td>
      <td align="center" valign="center"></td> <!-- 填写实际张数 -->
      <td align="center" valign="center"></td>
      <td align="center" valign="center"></td>
      <td align="center" valign="center">for test</td> <!-- 填写备注信息 -->
   </tr>
   <tr>
      <td align="center" valign="center">黃光區人員跌倒</td>
      <td align="center" valign="center"></td> <!-- 填写实际张数 -->
      <td align="center" valign="center"></td>
      <td align="center" valign="center"></td>
      <td align="center" valign="center">for test</td> <!-- 填写备注信息 -->
   </tr>